// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SpacePiratesCPPGameMode.h"
#include "SpacePiratesCPPPlayerController.h"
#include "SpacePiratesCPPCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASpacePiratesCPPGameMode::ASpacePiratesCPPGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASpacePiratesCPPPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}