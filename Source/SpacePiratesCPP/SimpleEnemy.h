// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy.h"
#include "SimpleEnemy.generated.h"

class ASpacePiratesCPPCharacter;

UCLASS()
class SPACEPIRATESCPP_API ASimpleEnemy : public AActor, public IEnemy
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASimpleEnemy();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void GetDamage(float Damage, ASpacePiratesCPPCharacter* PlayerCharacter) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	void DestroyEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	virtual void Move(float DeltaSeconds) override;

	FTimerHandle LifeDurationTimer;

	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* ExplosionTemplate;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf <class UCameraShake> CameraShakeClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* ExplotionSoundClass;

	void MakeExplotion();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor,
							UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
							bool bFromSweep, const FHitResult & SweepResult);
};
