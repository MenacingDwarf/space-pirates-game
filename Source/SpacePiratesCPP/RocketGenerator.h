// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RocketGenerator.generated.h"

UCLASS()
class SPACEPIRATESCPP_API ARocketGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARocketGenerator();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ASimpleEnemy> EnemyClass;

	float RocketTime;

	FTimerHandle RocketSpawnTimer;
	UPROPERTY(EditDefaultsOnly)
	float MinSpawnTime;
	UPROPERTY(EditDefaultsOnly)
	float MaxSpawnTime;

	UPROPERTY(EditDefaultsOnly)
	float SpawnProbability;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnRocket();

};
