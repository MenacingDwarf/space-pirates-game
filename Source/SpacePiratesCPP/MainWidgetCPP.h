// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainWidgetCPP.generated.h"

/**
 * 
 */
class UEditableTextBox;
UCLASS()
class SPACEPIRATESCPP_API UMainWidgetCPP : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite)
	UEditableTextBox* BulletsAmountTextBox;

protected:
	virtual bool Initialize() override;
};
