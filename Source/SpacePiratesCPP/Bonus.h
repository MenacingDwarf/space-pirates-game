// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Bonus.generated.h"

struct FHitResult;
class ASpacePiratesCPPCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBonus : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SPACEPIRATESCPP_API IBonus
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UStaticMeshComponent* MeshComponent;

	virtual void BonusAction(ASpacePiratesCPPCharacter* PlayerCharacter);
};
