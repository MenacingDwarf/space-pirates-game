// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

class UStaticMeshComponent;
class ASpacePiratesCPPCharacter;

UCLASS()
class SPACEPIRATESCPP_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Speed;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Damage;

	FVector Direction;
	ASpacePiratesCPPCharacter* PlayerRef;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Move(float DeltaTime);

	FTimerHandle LifeDurationTimer;

	void DestroyBullet();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor,
							UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
							bool bFromSweep, const FHitResult & SweepResult);

};
