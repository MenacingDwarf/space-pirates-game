// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SpacePiratesCPPPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "SpacePiratesCPPCharacter.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "GameFramework/SpringArmComponent.h"

ASpacePiratesCPPPlayerController::ASpacePiratesCPPPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ASpacePiratesCPPPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ASpacePiratesCPPPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ASpacePiratesCPPPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ASpacePiratesCPPPlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ASpacePiratesCPPPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ASpacePiratesCPPPlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ZoomIncrease", IE_Pressed, this, &ASpacePiratesCPPPlayerController::ZoomIncrease);
	InputComponent->BindAction("ZoomDecrease", IE_Pressed, this, &ASpacePiratesCPPPlayerController::ZoomDecrease);
	InputComponent->BindAction("Fire", IE_Pressed, this, &ASpacePiratesCPPPlayerController::FireHandler);
	InputComponent->BindAction("ResetVR", IE_Pressed, this, &ASpacePiratesCPPPlayerController::PauseGame);
}

void ASpacePiratesCPPPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(Hit.ImpactPoint);
	}
}

void ASpacePiratesCPPPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ASpacePiratesCPPPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ASpacePiratesCPPPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ASpacePiratesCPPPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
	StopMovement();
}

void ASpacePiratesCPPPlayerController::ChangeZoom(float ZoomDelta)
{
	ASpacePiratesCPPCharacter* MyPawn = Cast<ASpacePiratesCPPCharacter>(GetPawn());
	USpringArmComponent* CameraBoom = MyPawn->GetCameraBoom();
	CameraBoom->TargetArmLength = CameraBoom->TargetArmLength + ZoomDelta;
	CameraBoom->TargetArmLength = FMath::Clamp(CameraBoom->TargetArmLength, 200.0f, 1200.0f);
}

void ASpacePiratesCPPPlayerController::ZoomIncrease()
{
	ChangeZoom(-100.0f);
}

void ASpacePiratesCPPPlayerController::ZoomDecrease()
{
	ChangeZoom(100.0f);
}

void ASpacePiratesCPPPlayerController::FireHandler()
{
	ASpacePiratesCPPCharacter* PlayerCharacter = Cast<ASpacePiratesCPPCharacter>(GetPawn());
	PlayerCharacter->Fire();
}

void ASpacePiratesCPPPlayerController::PauseGame()
{
	SetPause(true);
}
