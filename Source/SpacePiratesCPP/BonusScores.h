// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bonus.h"
#include "BonusScores.generated.h"

class ASpacePiratesCPPCharacter;

UCLASS()
class SPACEPIRATESCPP_API ABonusScores : public AActor, public IBonus
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusScores();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float ScoresAmount;

	UPROPERTY(BlueprintReadWrite)
	float LifeDurationTime;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* PickUpSoundClass;

	FTimerHandle LifeDurationTimer;

	virtual void BonusAction(ASpacePiratesCPPCharacter* PlayerCharacter) override;

private:
	void DestroyBonus();
};
