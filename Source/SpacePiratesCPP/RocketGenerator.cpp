// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketGenerator.h"
#include "Components/MeshComponent.h"
#include "SimpleEnemy.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ARocketGenerator::ARocketGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MinSpawnTime = 15.f;
	MaxSpawnTime = 30.f;
	SpawnProbability = 0.5f;
	RocketTime = FMath::RandRange(MinSpawnTime, MaxSpawnTime);
}

// Called when the game starts or when spawned
void ARocketGenerator::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(RocketSpawnTimer, this, &ARocketGenerator::SpawnRocket, RocketTime, false);
}

// Called every frame
void ARocketGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARocketGenerator::SpawnRocket()
{	
	// Recount new rocket spawn time between min and max
	RocketTime = FMath::RandRange(MinSpawnTime, MaxSpawnTime);
	
	// With spawn probability new rocket will be spawn in generator location and rotation
	if (UKismetMathLibrary::RandomBoolWithWeight(SpawnProbability))
	{
		ASimpleEnemy* NewRocket = GetWorld()->SpawnActor<ASimpleEnemy>(EnemyClass, FTransform(GetActorRotation(), GetActorLocation()));
	}

	// Increase spawn probability and reduce min and max spawn time to make game more harder
	SpawnProbability = FMath::Clamp(SpawnProbability + 0.05f, 0.f, 1.f);
	MinSpawnTime = FMath::Clamp(MinSpawnTime - 0.4f, 2.f, 30.f);
	MaxSpawnTime = FMath::Clamp(MaxSpawnTime - 0.4f, 7.f, 60.f);

	// Restart timer
	GetWorld()->GetTimerManager().SetTimer(RocketSpawnTimer, this, &ARocketGenerator::SpawnRocket, RocketTime, false);
}

