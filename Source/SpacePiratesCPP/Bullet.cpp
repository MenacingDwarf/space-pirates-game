// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/MeshComponent.h"
#include "Enemy.h"
#include "SpacePiratesCPPCharacter.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABullet::HandleBeginOverlap);

	Speed = 1500.f;
	Damage = 1.f;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	Direction = UKismetMathLibrary::Conv_RotatorToVector(GetActorRotation());
	GetWorld()->GetTimerManager().SetTimer(LifeDurationTimer, this, &ABullet::DestroyBullet, 5.f, false);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void ABullet::Move(float DeltaTime)
{
	FVector DeltaLocation = DeltaTime * Speed * Direction;
	AddActorWorldOffset(DeltaLocation);
}

void ABullet::DestroyBullet()
{
	Destroy();
}

void ABullet::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor,
								 UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, 
								 bool bFromSweep, const FHitResult & SweepResult)
{
	IEnemy* Enemy = Cast<IEnemy>(OtherActor);
	if (Enemy != nullptr)
	{
		Enemy->GetDamage(Damage, PlayerRef);
		Destroy();
	}
}

