// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleEnemy.h"
#include "Components/MeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "SpacePiratesCPPCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/PlayerCameraManager.h"
#include "SpacePiratesCPPCharacter.h"
#include "Sound/SoundBase.h"

// Sets default values
ASimpleEnemy::ASimpleEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initializing main enemy characteristics
	Direction = { 1, 0, 0 };
	Speed = 900;
	RewardScores = 1.f;

	// Initializing mesh component with overlap collision
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASimpleEnemy::HandleBeginOverlap);
}

// Called when the game starts or when spawned
void ASimpleEnemy::BeginPlay()
{
	Super::BeginPlay();
	Direction = UKismetMathLibrary::Conv_RotatorToVector(GetActorRotation());
	GetWorld()->GetTimerManager().SetTimer(LifeDurationTimer, this, &ASimpleEnemy::DestroyEnemy, 10.f, false);
}

// Called every frame
void ASimpleEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void ASimpleEnemy::GetDamage(float Damage, ASpacePiratesCPPCharacter* PlayerCharacter)
{
	PlayerCharacter->ReceiveScores(RewardScores);
	MakeExplotion();
	Destroy();
}

void ASimpleEnemy::Move(float DeltaSeconds)
{
	FVector DeltaLocation = DeltaSeconds * Speed * Direction;
	AddActorWorldOffset(DeltaLocation);
}

void ASimpleEnemy::MakeExplotion()
{
	// Sound explotion effect and camera shake
	UGameplayStatics::PlaySound2D(GetWorld(), ExplotionSoundClass);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionTemplate, FTransform(GetActorLocation()), true, EPSCPoolMethod::None, true);
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CameraShakeClass, 1.f);
}

void ASimpleEnemy::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ASpacePiratesCPPCharacter* PlayerCharacter = Cast<ASpacePiratesCPPCharacter>(OtherActor);
	if (IsValid(PlayerCharacter))
	{
		PlayerCharacter->ReceiveDamage(1.0f);
	}
}

void ASimpleEnemy::DestroyEnemy()
{
	Destroy();
}

