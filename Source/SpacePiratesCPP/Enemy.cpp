// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "SpacePiratesCPPCharacter.h"

// Add default functionality here for any IEnemy functions that are not pure virtual.

void IEnemy::GetDamage(float Damage, ASpacePiratesCPPCharacter* PlayerCharacter)
{
}

void IEnemy::Move(float DeltaSeconds)
{
}
