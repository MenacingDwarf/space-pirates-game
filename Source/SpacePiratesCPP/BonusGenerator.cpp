// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusGenerator.h"
#include "Bonus.h"
#include "BonusScores.h"
#include "Engine/Engine.h"

// Sets default values
ABonusGenerator::ABonusGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnTime = 5.f;
}

// Called when the game starts or when spawned
void ABonusGenerator::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(BonusSpawnTimer, this, &ABonusGenerator::SpawnBonus, SpawnTime, false);
}

// Called every frame
void ABonusGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusGenerator::SpawnBonus()
{
	// Get world coordinates of angles
	FVector WorldFirstLocation = GetActorLocation() + FirstAngle;
	FVector WorldSecondLocation = GetActorLocation() + SecondAngle;

	// Random location in space between angles
	float XBonusLocation = FMath::RandRange(WorldFirstLocation.X, WorldSecondLocation.X);
	float YBonusLocation = FMath::RandRange(WorldFirstLocation.Y, WorldSecondLocation.Y);
	float ZBonusLocation = FMath::RandRange(WorldFirstLocation.Z, WorldSecondLocation.Z);
	FVector BonusLocation(XBonusLocation, YBonusLocation, ZBonusLocation);

	// Generate new bonus and restart timer
	ABonusScores* NewBonus = GetWorld()->SpawnActor<ABonusScores>(BonusClass, FTransform(BonusLocation));
	GetWorld()->GetTimerManager().SetTimer(BonusSpawnTimer, this, &ABonusGenerator::SpawnBonus, SpawnTime, false);
}

