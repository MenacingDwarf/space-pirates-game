// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SpacePiratesCPPCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Bullet.h"
#include "MainWidgetCPP.h"
#include "SpacePiratesCPPPlayerController.h"
#include "Bonus.h"

ASpacePiratesCPPCharacter::ASpacePiratesCPPCharacter()
{
	BulletsAmount = 5;
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ASpacePiratesCPPCharacter::HandleBeginOverlap);

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ASpacePiratesCPPCharacter::BeginPlay()
{
	Super::BeginPlay();
	ASpacePiratesCPPPlayerController* PC = Cast<ASpacePiratesCPPPlayerController>(GetController());
	PC->SetPause(true);
}

void ASpacePiratesCPPCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FRotator NewRotation;
			FVector NewLocation;
			ToCursorLocationAndRotation(NewLocation, NewRotation);
			SetActorRotation(NewRotation);

			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void ASpacePiratesCPPCharacter::ToCursorLocationAndRotation(FVector & Location, FRotator& Rotation)
{
	if (CursorToWorld != nullptr)
	{
		// Count cursor location with fixed Z coordinate
		FVector CursorLocation = CursorToWorld->GetComponentLocation();
		FVector NewLocation = { CursorLocation.X, CursorLocation.Y, GetActorLocation().Z };

		// New location is counted as point in 50 distance on vector between cursor and character
		NewLocation = NewLocation - GetActorLocation();
		NewLocation.Normalize();
		NewLocation = NewLocation * 50.0f + GetActorLocation();

		// Write new values in Location and Rotation
		Location = NewLocation;
		Rotation = UKismetMathLibrary::Conv_VectorToRotator(NewLocation - GetActorLocation());
	}
}

void ASpacePiratesCPPCharacter::HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	IBonus* Bonus = Cast<IBonus>(OtherActor);
	if (Bonus)
	{
		Bonus->BonusAction(this);
	}
}

void ASpacePiratesCPPCharacter::Fire()
{
	if (BulletsAmount > 0)
	{
		// Receive bullet transform according to the cursor position
		FVector BulletLocation;
		FRotator BulletRotation;
		ToCursorLocationAndRotation(BulletLocation, BulletRotation);

		// Spawn new bullet with sound and reduce available bullets amount
		ABullet* NewBullet = GetWorld()->SpawnActor<ABullet>(BulletClass, FTransform(BulletRotation, BulletLocation));
		if (IsValid(NewBullet))
		{
			UGameplayStatics::PlaySound2D(GetWorld(), ShootingSoundClass);
			NewBullet->PlayerRef = this;
			BulletsAmount--;
		}
	}
}

void ASpacePiratesCPPCharacter::ReceiveBulletsOnTime()
{
	ReceiveBullets();
	GetWorld()->GetTimerManager().SetTimer(BulletReloadHandle, this, &ASpacePiratesCPPCharacter::ReceiveBulletsOnTime, 5.f, false);
}

void ASpacePiratesCPPCharacter::ReceiveBullets(int ReceivableBulletsAmount)
{
	BulletsAmount += ReceivableBulletsAmount;
}

void ASpacePiratesCPPCharacter::ReceiveDamage(float Damage)
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	PC->SetPause(true);
	MenuWidget = CreateWidget<UUserWidget>(PC, MenuWidgetClass);
	MenuWidget->AddToViewport();
}

void ASpacePiratesCPPCharacter::ReceiveScores(float AddScores)
{
	Scores += AddScores;
	// GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Scores: " + FString::SanitizeFloat(Scores)));
}

void ASpacePiratesCPPCharacter::ShowWidget()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	UUserWidget* MainWidget = CreateWidget<UUserWidget>(PC, MainWidgetClass, TEXT("MainWidget"));
	MainWidget->AddToViewport();
}

void ASpacePiratesCPPCharacter::StartGame()
{
	GetWorld()->GetTimerManager().SetTimer(BulletReloadHandle, this, &ASpacePiratesCPPCharacter::ReceiveBulletsOnTime, 5.f, false);
	ShowWidget();
	ASpacePiratesCPPPlayerController* PC = Cast<ASpacePiratesCPPPlayerController>(GetController());
	PC->SetPause(false);
}


