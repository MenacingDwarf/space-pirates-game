// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SpacePiratesCPPCharacter.generated.h"

class ABullet;

UCLASS(Blueprintable)
class ASpacePiratesCPPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASpacePiratesCPPCharacter();

	UPROPERTY(BlueprintReadWrite)
	int BulletsAmount;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABullet> BulletClass;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float Scores;

	UFUNCTION()
	void Fire();

	void ReceiveBulletsOnTime();

	UFUNCTION(BlueprintCallable)
	void ReceiveBullets(int ReceivableBulletsAmount = 1);

	void ReceiveDamage(float Damage);

	UFUNCTION(BlueprintCallable)
	void ReceiveScores(float AddScores = 1.f);

	UFUNCTION(BlueprintCallable)
	void ShowWidget();

	UFUNCTION(BlueprintCallable)
	void StartGame();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> MainWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> MenuWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* ShootingSoundClass;


	UUserWidget* MenuWidget;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	void ToCursorLocationAndRotation(FVector& Location, FRotator& Rotation);

	FTimerHandle BulletReloadHandle;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent * OverlapComponent, AActor * OtherActor,
			UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult & SweepResult);
};

