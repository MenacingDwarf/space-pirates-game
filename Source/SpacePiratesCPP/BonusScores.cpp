// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusScores.h"
#include "Components/StaticMeshComponent.h"
#include "SpacePiratesCPPCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABonusScores::ABonusScores()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	ScoresAmount = 1.f;
	LifeDurationTime = 10.f;
}

// Called when the game starts or when spawned
void ABonusScores::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(LifeDurationTimer, this, &ABonusScores::DestroyBonus, LifeDurationTime, false);
}

// Called every frame
void ABonusScores::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusScores::BonusAction(ASpacePiratesCPPCharacter * PlayerCharacter)
{
	UGameplayStatics::PlaySound2D(GetWorld(), PickUpSoundClass);
	PlayerCharacter->ReceiveScores(ScoresAmount);
	DestroyBonus();
}

void ABonusScores::DestroyBonus()
{
	Destroy();
}

