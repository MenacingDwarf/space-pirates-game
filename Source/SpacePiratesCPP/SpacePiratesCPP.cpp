// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SpacePiratesCPP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpacePiratesCPP, "SpacePiratesCPP" );

DEFINE_LOG_CATEGORY(LogSpacePiratesCPP)
 